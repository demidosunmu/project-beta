from common.json import ModelEncoder

from .models import Appointment, AutomobileVO, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id'
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ['vin', 'color', 'year']


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'customer',
        'technician',
        'vin',
        "vip_status",

    ]
    encoders = {
        'technician': TechnicianEncoder(),
    }
