import { useState, useEffect } from 'react';


function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async ()=> {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments)
        }
        }

    useEffect(() => {
        fetchData();
      }, []);

    const canceledAppointment = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/cancel`;
        const fetchConfig = {
                method: "put",
                headers: {
                    "Content-Type": "application/json",
                },
            };

            const response = await fetch(url, fetchConfig);
            const data = await response.json();
            console.log(data)

            window.location.reload();

        }



    const finishedAppointment = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/finish`;
        const fetchConfig = {
            method: "put",
			body: JSON.stringify({ finished: true }),
			headers: {
				'Content-Type': 'application/json',
			}
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json();
        console.log(data)

        window.location.reload();

    }


    return (
        <div>
            <h1> Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                {appointments.map(appointment => {
                    if (appointment.status === 'created')
                        return (
                            <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip_status === true ? "yes" : "no"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>
                                <button onClick={(e) => canceledAppointment(appointment.id)} style={{backgroundColor: "red"}} className="btn btn-primary">Cancel</button>
                            </td>
                            <td>
                                <button onClick={(e) => finishedAppointment(appointment.id)} style={{backgroundColor: "green"}} className="btn btn-primary">Finished</button>
                            </td>
                        </tr>
                        )
                    }

                )}
                </tbody>
            </table>
        </div>
    )
            }

export default AppointmentList;
