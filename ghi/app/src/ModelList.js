import React, { useEffect, useState } from 'react'

function ModelList() {
    const [models, setModels] = useState([]);

    const handleImage = (event) => {
        event.target.src = 'https://i1.sndcdn.com/avatars-000225352686-ngevdh-t500x500.jpg';
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
      }, []);

    return (
        <>
            <h1>Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={ model.id }>
                                <td>{ model.name} </td>
                                <td>{ model.manufacturer.name }</td>
                                <td><img src={model.picture_url} style={{height: 200, width: 300}} onError={handleImage} /></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default ModelList
